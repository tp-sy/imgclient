# Scanning text from images
The system needs a rabbitmq queue, server and 1+ scanner containers

Running rabbitmq queue:

    docker run -d --hostname imgrabbit --name jano -p 8080:15672 -p 8081:5672 rabbitmq:3-management

The queue will then be located in port 8081

Running the imgserver:

    # In the imgserver root
    docker build . -t imgeserver
    docker run -it --network=host imgserver

Running imgscanner:

    # In the imgscanner root
    docker build . -t imgscanner
    docker run -it --network=host imgserver

You can then scan images using:

    client.py 127.0.0.1:8000 <filepath to png file>