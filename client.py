import requests
import json
import time
from argparse import ArgumentParser

if __name__=="__main__":
    parser = ArgumentParser()
    parser.add_argument("server", type=str, help="location of the server")
    parser.add_argument("filepaths", nargs="+", type=str, help="list of files to be scanned")
    args = parser.parse_args()
    scans = dict()
    for filepath in args.filepaths:
        # http://127.0.0.1:8000/img/new_scan/
        res = requests.post(
            url=args.server + "/img/new_scan/",
            files=dict(
                file=open(filepath,"rb"),
            ),
        )
        if res.status_code == 200:
            scans[filepath] = \
                {   
                    "uuid": res.content.decode("utf-8", "ignore"),
                    "status": 0,
                    "check_count": 0
                }
            print("started scan for file", filepath, "with id", res.content.decode("utf-8", "ignore"))
        else:
            print("Failed to post scan", res.content)
        time.sleep(0.5)
    completed = dict()
    tries = 0
    while True:
        tries += 1
        # curl -X GET -i http://127.0.0.1:8000/img/scan_status/3c717895-0347-4d38-9fb7-f3b31a8abaea/ 
        for filepath, scan in scans.items():
            if scan["status"] == 0:
                scan["check_count"] += 1
                if scan["check_count"] > 10000:
                    scan["status"] = "SERVER_UNRESPONSIVE"
                res = requests.get(
                    url=args.server + "/img/scan_status/" + scan["uuid"] + "/"
                )
                current_status = json.loads(res.content)
                if current_status.get("status", "IN_PROGRESS") != "IN_PROGRESS":
                    print("Completed scan: ", current_status.get("status"))
                    print("Text:", current_status.get("text"))
                    print("Time:", scan["check_count"] * 0.2, "seconds")
                    completed[filepath] = scan
                    scan["status"] = current_status.get("status")
                
        if len(scans) == len(completed):
            print("Finished all scans")
            break
        time.sleep(0.2)

